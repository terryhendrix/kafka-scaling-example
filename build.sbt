import sbt._

lazy val root = Project(id = "kafka-scaling-example", base = file("."))
  .aggregate(Build.consumer, Build.producer)
  .settings(
      organization := "nl.terry.hendrix",
      version := "0.1.0-SNAPSHOT",
      name := "tractracer",
      scalaVersion in ThisBuild := "2.11.7"
  )
