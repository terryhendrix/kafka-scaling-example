package sample.reactivekafka

import akka.actor._
import akka.kafka.scaladsl._
import akka.kafka.{ ConsumerSettings, Subscriptions }
import akka.stream._
import akka.stream.scaladsl.Sink
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.{ ByteArrayDeserializer, StringDeserializer }

import scala.language.postfixOps
import scala.util.Try

object TickConsumer extends App {

  implicit val system = ActorSystem("TickConsumer")
  implicit val mat = ActorMaterializer()
  implicit val ec = system.dispatcher

  val kafkaUrl = system.settings.config.getString("app.kafka-url")
  val topic = system.settings.config.getString("app.topic")
  val consumerGroup = system.settings.config.getString("app.consumer-group")
  val maxPartitions = system.settings.config.getInt("app.max-partitions")

  val consumerSettings = ConsumerSettings(system, new ByteArrayDeserializer, new StringDeserializer)
    .withGroupId(consumerGroup)
    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    .withBootstrapServers(kafkaUrl)

  def primesUnder(n: Int): List[Int] = {
    def prime(num: Int, factors: List[Int]): Boolean = {
      factors.forall(num % _ != 0)
    }

    require(n >= 2)

    def rec(i: Int, primes: List[Int]): List[Int] = {
      if (i >= n) primes
      else if (prime(i, primes)) rec(i + 1, i :: primes)
      else rec(i + 1, primes)
    }

    rec(2, List()).reverse
  }

  val consume = Consumer.committablePartitionedSource(consumerSettings, Subscriptions.topics(topic))
    .flatMapMerge(maxPartitions, _._2)
    .map { msg =>
      system.log.info(s"Consumed $msg")
      Try(msg.record.value().toInt)
        .map(primesUnder)
        .map(pr ⇒ system.log.info(s"${pr.size} primes under ${msg.record.value} "))
      msg
    }
    .mapAsync(1) { msg ⇒
      msg.record.value().hashCode
      msg.committableOffset.commitScaladsl()
    }

  consume.runWith(Sink.ignore).map { done ⇒
    system.log.info(s"Done consuming: $done")
  }.recover {
    case ex ⇒
      ex.printStackTrace()
  }
}
