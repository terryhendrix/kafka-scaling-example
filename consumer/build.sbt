import sbt.Keys._
import scalariform.formatter.preferences._

val akkaVersion = "2.4.11"
val akkaStreamVersion = "2.0"
val kafkaVersion = "0.9.0.0"

lazy val consumer = (project in file("."))
  .enablePlugins(JavaAppPackaging, sbtdocker.DockerPlugin)
  .settings(
    name := "consumer",
    version := "0.1.0",
    organization := "terryhendrix",
    scalaVersion := "2.11.7",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream-kafka" % "0.12",
      "ch.qos.logback" % "logback-classic" % "1.1.3",
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "org.scalatest" %% "scalatest" % "2.2.4" % "test",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
    ),
    scalariformSettings,
    ScalariformKeys.preferences := ScalariformKeys.preferences.value
      .setPreference(AlignSingleLineCaseStatements, true)
      .setPreference(AlignSingleLineCaseStatements.MaxArrowIndent, 100)
      .setPreference(DoubleIndentClassDeclaration, true)
      .setPreference(PreserveDanglingCloseParenthesis, true),
    fork in run := true,
    packageName in Docker := "terryhendrix/"+name.value
  )

