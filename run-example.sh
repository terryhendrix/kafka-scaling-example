#!/bin/bash

# Wait for a certain service to become available
waitForService() {
while true; do
  if ! nc -z $1 $2
  then
    echo "Service $1:$2 not available, retrying..."
    sleep 1
  else
    echo "Service $1:$2 is available"
    break;
  fi
done;
}

docker rm -f $(docker ps -aq)
sbt docker:publishLocal
docker-compose scale consumer_neo=3 consumer_trinity=3 producer=1 kafka=1 zookeeper=1

waitForService localhost 2181
waitForService localhost 2888
waitForService localhost 3888
waitForService localhost 7203
waitForService localhost 9092

docker-compose up --force-recreate

