# Kafka scaling example

A small try out project to check how kafka works.
Please download the yED editor to checkout the Design.graphml


## Example:

To start kafka, zookeeper, producers and consumers: please run ./run-exampe.sh
A topic with 1 partition will be created. To increase the partitions please run ./increase-partitions.sh
10 partitions will now exist, they will be load balanced over the consumers in a group.
