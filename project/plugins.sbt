addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.4")

addSbtPlugin("com.typesafe.akka" % "akka-sbt-plugin" % "2.2.4")

addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.3.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.0-RC1")

resolvers += "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases/"

