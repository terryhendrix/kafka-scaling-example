import sbt._

object Build {
  lazy val consumer = RootProject(file("consumer"))
  lazy val producer = RootProject(file("producer"))
}
