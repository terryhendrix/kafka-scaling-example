#!/bin/bash

ZK_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' kafkaexchangeexample_zookeeper_1)
KAFKA_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' kafkaexchangeexample_kafka_1)
docker run --rm ches/kafka kafka-topics.sh --alter --topic DigitTopic --partitions 10 --zookeeper $ZK_IP:2181