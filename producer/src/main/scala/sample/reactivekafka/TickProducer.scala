package sample.reactivekafka

import akka.actor._
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl._
import akka.stream._
import akka.stream.scaladsl.Source
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ ByteArraySerializer, StringSerializer }

import scala.concurrent.duration._
import scala.language.postfixOps

object TickProducer extends App {

  implicit val system = ActorSystem("TickProducer")
  implicit val mat = ActorMaterializer()
  implicit val ec = system.dispatcher

  val kafkaUrl = system.settings.config.getString("app.kafka-url")
  val topic = system.settings.config.getString("app.topic")

  val producerSettings = ProducerSettings(system, new ByteArraySerializer, new StringSerializer)
    .withBootstrapServers(kafkaUrl)

  var ticks = 0
  var partition = 0

  val produce = Source.tick(0 seconds, 1 second, 1234)
    .buffer(10, OverflowStrategy.backpressure)
    .map { _ ⇒
      ticks = ticks + 10
      ticks
    }
    .map(_.toString())
    .map { elem =>
      val msg = new ProducerRecord[Array[Byte], String](topic, elem.hashCode.toString.getBytes, elem)
      system.log.info(s"Producing $msg")
      msg
    }

  produce.runWith(Producer.plainSink(producerSettings)).map { done ⇒
    system.log.info(s"Done producing: $done")
  }.recover {
    case ex ⇒
      ex.printStackTrace()
  }
}
